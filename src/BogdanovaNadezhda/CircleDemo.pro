#-------------------------------------------------
#
# Project created by QtCreator 2016-03-23T22:43:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CircleDemo
TEMPLATE = app


SOURCES += main.cpp \
    Circle.cpp

HEADERS  += \
    Circle.h

FORMS    += mainwindow.ui
