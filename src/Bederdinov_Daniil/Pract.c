#include <stdio.h>
#include <math.h>

int main()
{
	float a = 0;
	float b = 0;
	float c = 0;
	float p = 0;
	float S = 0;
	float tax = 0;
	float trate = 0;

	printf("Triangle tax v. 0.99 \n");
	printf("Enter a b c \n");
	scanf_s("%f %f %f", &a, &b, &c);
	if (a>0)
	{
		if (b>0)
		{
			if (c>0)
			{
				p = (a + b + c) / 2;
				S = sqrt(p*(p - a)*(p - b)*(p - c));
				//printf("S = %f \n",S);
				printf("Enter tax rate \n");
				scanf_s("%f",&trate);
				if (trate > 0)
				{
					tax = S*trate;
					printf("Tax is %f \n", tax);
					return 0;
				}
				else
				{
					printf("Wrong tax value \n");
					return 1;
				}
			}
			else
			{
				printf("Wrong c value \n");
				return 1;
			}
		}
		else
		{
			printf("Wrong b value \n");
			return 1;
		}
	}
	else
	{
		printf("Wrong a value \n");
		return 1;
	}
}